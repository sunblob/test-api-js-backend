const mongoose = require('mongoose');
const logger = require('../services/logger')(module);

const dbConnect = async () => {
  const connection = await mongoose.connect(`${process.env.MONGO_URI}`, {
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASSWORD,
  });

  logger.info(`MongoDB connected ${connection.connection.host}...`);
};

module.exports = dbConnect;
