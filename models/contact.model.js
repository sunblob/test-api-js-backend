const mongoose = require('mongoose');

const ContactSchema = mongoose.Schema(
  {
    lastname: {
      type: String,
      required: true,
    },
    firstname: {
      type: String,
      required: true,
    },
    patronymic: {
      type: String,
    },
    phone: {
      type: String,
      match: [/^7\d{10}$/, 'Введите валидный номер телефона'],
      required: true,
    },
    email: {
      type: String,
      match: [/^[^\s@]+@[^\s@]+\.[^\s@]+$/, 'Введите валидный email'],
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Contact', ContactSchema);
