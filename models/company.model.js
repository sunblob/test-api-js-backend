const mongoose = require('mongoose');

const CompanySchema = mongoose.Schema(
  {
    contactId: {
      type: mongoose.Types.ObjectId,
      ref: 'Contact',
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    shortName: {
      type: String,
      required: true,
    },
    businessEntity: {
      type: String,
      required: true,
    },
    contract: {
      no: String,
      issue_date: Date,
    },
    type: {
      type: String,
      enum: ['agent', 'contractor'],
      required: true,
    },
    status: {
      type: String,
      enum: ['active'],
      default: 'active',
    },
    address: {
      type: String,
    },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Company', CompanySchema);
