/* eslint-env jest */
const supertest = require('supertest');
const jwt = require('jsonwebtoken');
const { MongoMemoryServer } = require('mongodb-memory-server');
const mongoose = require('mongoose');

const config = require('../config.json');
const createApp = require('../utils/create-app');
const { create } = require('../services/companies.service');
const { create: createContact } = require('../services/contacts.service');

const app = createApp();

const userId = new mongoose.Types.ObjectId().toString();

const companyPayload = {
  contactId: 'Test',
  name: 'TESTING',
  shortName: 'TEST',
  businessEntity: 'OOO',
  type: 'agent',
  status: 'active',
  contract: {
    no: '131231',
    issue_date: Date.now(),
  },
  address: 'ulica',
};

const contactPayload = {
  firstname: 'Test',
  lastname: 'jest',
  patronymic: 'supertest',
  phone: '79992667733',
  email: 'test@test.com',
};

const userPayload = {
  _id: userId,
  login: 'test',
  password: 'p4ssw0rd',
};

describe('company', () => {
  beforeAll(async () => {
    const mongoServer = await MongoMemoryServer.create();

    await mongoose.connect(mongoServer.getUri());
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoose.connection.close();
  });

  describe('create company', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const { statusCode } = await supertest(app).post('/companies');

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and create new company', async () => {
        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const contact = await createContact(contactPayload);

        companyPayload.contactId = contact.id;
        const { statusCode } = await supertest(app).post('/companies').set('Authorization', `Bearer ${token}`).send(companyPayload);

        expect(statusCode).toBe(200);
      });
    });
  });

  describe('get company by id', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const company = await create(companyPayload);

        const { statusCode } = await supertest(app).get(`/companies/${company.id}`);

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and get company by id', async () => {
        const company = await create(companyPayload);

        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const { statusCode } = await supertest(app).get(`/companies/${company.id}`).set('Authorization', `Bearer ${token}`);

        expect(statusCode).toBe(200);
      });
    });
  });

  describe('get all companies route', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const { statusCode } = await supertest(app).get('/companies');

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and array of companies', async () => {
        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const { statusCode } = await supertest(app).get('/companies').set('Authorization', `Bearer ${token}`);

        expect(statusCode).toBe(200);
      });
    });
  });

  describe('delete company by id', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const company = await create(companyPayload);

        const { statusCode } = await supertest(app).delete(`/companies/${company.id}`);

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and delete new company', async () => {
        const company = await create(companyPayload);

        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const { statusCode } = await supertest(app).delete(`/companies/${company.id}`).set('Authorization', `Bearer ${token}`);

        expect(statusCode).toBe(200);
      });
    });
  });
});
