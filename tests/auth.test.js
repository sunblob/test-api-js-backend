/* eslint-env jest */
const supertest = require('supertest');
const mongoose = require('mongoose');
const authService = require('../services/auth.service');
const createApp = require('../utils/create-app');

const app = createApp();

const userId = new mongoose.Types.ObjectId().toString();

const authPayload = {
  userId,
  accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjYyZDcyYTY1NWZjNmRjZDQzZjU0NWU5OCIsImxvZ2luIjoia2VrIiwicGFzc3dvcmQiOiIxMjM0NTYiLCJjcmVhdGVkQXQiOiIyMDIyLTA3LTE5VDIyOjA0OjIxLjI4M1oiLCJ1cGRhdGVkQXQiOiIyMDIyLTA3LTE5VDIyOjA0OjIxLjI4M1oiLCJfX3YiOjB9LCJpYXQiOjE2NTgzMjc3NTIsImV4cCI6MTY1ODkzMjU1Mn0.JYZhgaLP-YRiasvvaMQW8bSLR49B9u-IlBvpy1bEeqE',
};

const userInput = {
  login: 'test',
  password: 'p4ssw0rd',
};

describe('auth', () => {
  describe('user login', () => {
    it('should return the token and userId', async () => {
      const authServiceMock = jest.spyOn(authService, 'loginUser').mockReturnValueOnce(authPayload);

      const { statusCode, body } = await supertest(app).post('/auth/login').send(userInput);

      expect(statusCode).toBe(200);

      expect(body).toEqual(authPayload);

      expect(authServiceMock).toHaveBeenCalledWith(userInput);
    });
  });
});
