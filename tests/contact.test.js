/* eslint-env jest */
const supertest = require('supertest');
const jwt = require('jsonwebtoken');
const { MongoMemoryServer } = require('mongodb-memory-server');
const mongoose = require('mongoose');

const config = require('../config.json');
const createApp = require('../utils/create-app');
const { create } = require('../services/contacts.service');

const app = createApp();

const userId = new mongoose.Types.ObjectId().toString();

const contactPayload = {
  firstname: 'Test',
  lastname: 'jest',
  patronymic: 'supertest',
  phone: '79992667733',
  email: 'test@test.com',
};

const userPayload = {
  _id: userId,
  login: 'test',
  password: 'p4ssw0rd',
};

describe('contact', () => {
  beforeAll(async () => {
    const mongoServer = await MongoMemoryServer.create();

    await mongoose.connect(mongoServer.getUri());
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoose.connection.close();
  });

  describe('create contact', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const { statusCode } = await supertest(app).post('/contacts');

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and create new contact', async () => {
        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const { statusCode } = await supertest(app).post('/contacts').set('Authorization', `Bearer ${token}`).send(contactPayload);

        expect(statusCode).toBe(200);
      });
    });
  });

  describe('get contact by id', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const contact = await create(contactPayload);

        const { statusCode } = await supertest(app).get(`/contacts/${contact.id}`);

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and get contact by id', async () => {
        const contact = await create(contactPayload);

        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const { statusCode } = await supertest(app).get(`/contacts/${contact.id}`).set('Authorization', `Bearer ${token}`);

        expect(statusCode).toBe(200);
      });
    });
  });

  describe('get all contacts route', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const { statusCode } = await supertest(app).get('/contacts');

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and array of contacts', async () => {
        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const { statusCode } = await supertest(app).get('/contacts').set('Authorization', `Bearer ${token}`);

        expect(statusCode).toBe(200);
      });
    });
  });

  describe('delete contact by id', () => {
    describe('user is not logged in', () => {
      it('should return 401', async () => {
        const contact = await create(contactPayload);

        const { statusCode } = await supertest(app).delete(`/contacts/${contact.id}`);

        expect(statusCode).toBe(401);
      });
    });

    describe('user IS logged in', () => {
      it('should return 200 and delete new contact', async () => {
        const contact = await create(contactPayload);

        const token = jwt.sign({ userPayload }, config.app, {
          expiresIn: config.jwt_ttl,
        });

        const { statusCode } = await supertest(app).delete(`/contacts/${contact.id}`).set('Authorization', `Bearer ${token}`);

        expect(statusCode).toBe(200);
      });
    });
  });
});
