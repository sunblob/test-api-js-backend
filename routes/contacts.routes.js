const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth.middleware');
const contactsController = require('../controllers/contacts.controller');

router.get('/:id', auth, contactsController.getContact);

router.get('/', auth, contactsController.getContacts);

router.patch('/:id', auth, contactsController.updateContact);

router.post('/', auth, contactsController.createContact);

router.delete('/:id', auth, contactsController.deleteContact);

module.exports = router;
