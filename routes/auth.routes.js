const { Router } = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const logger = require('../services/logger')(module);
const authController = require('../controllers/auth.controller');

const router = Router();

router.get('/', (req, res) => {
  // eslint-disable-next-line no-unsafe-optional-chaining
  const { user } = req?.query;

  if (!user) {
    logger.error('No user passed');
    return res.status(400).json({
      error: 'No user passed',
    });
  }

  const token = jwt.sign({ user }, config.app, {
    expiresIn: config.jwt_ttl,
  });

  res.header('Authorization', `Bearer ${token}`);
  return res.status(200).end();
});

router.post('/login', authController.login);

router.post('/register', authController.register);

module.exports = router;
