const Company = require('../models/company.model');
const HttpException = require('../utils/http-exception');

async function get(id) {
  const company = await Company.findById(id);

  if (!company) {
    throw new HttpException(`Company with id# ${id} not found`, 404);
  }

  return company;
}

async function getAll(query) {
  return Company.find()
    .where(query.filter)
    .skip(query.page * query.limit)
    .limit(query.limit)
    .sort(query.sorting.join(' '));
}

async function create(createObj) {
  return Company.create(createObj);
}

async function del(id) {
  const company = await get(id);

  await company.delete();
}

async function update(id, updObj) {
  const company = await get(id);

  await company.update(updObj, { new: true });

  return company;
}

module.exports = {
  get,
  getAll,
  create,
  del,
  update,
};
