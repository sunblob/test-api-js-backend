const Contact = require('../models/contact.model');
const HttpException = require('../utils/http-exception');

async function get(id) {
  const contact = await Contact.findById(id);

  if (!contact) {
    throw new HttpException(`Contact with id# ${id} not found`, 404);
  }

  return contact;
}

async function getAll() {
  return Contact.find();
}

async function create(createObj) {
  return Contact.create(createObj);
}

async function del(id) {
  const contact = await get(id);

  await contact.delete();
}

async function update(id, updObj) {
  const contact = await get(id);

  await contact.update(updObj, { new: true });

  return contact;
}

module.exports = {
  get,
  getAll,
  create,
  del,
  update,
};
