const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const HttpException = require('../utils/http-exception');
const User = require('../models/user.model');
const config = require('../config.json');

async function loginUser(login, password) {
  const user = await User.findOne().where({
    login,
  });

  if (!user) {
    return new HttpException('User not found', 404);
  }

  const passwordsMatch = await bcrypt.compare(password, user.password);

  if (!passwordsMatch) {
    return new HttpException('Wrong password', 400);
  }

  const token = jwt.sign({ user }, config.app, {
    expiresIn: config.jwt_ttl,
  });

  return {
    token,
    userId: user.id,
  };
}

async function registerUser(login, password) {
  let user = await User.findOne().where({
    login,
  });

  if (user) {
    return new HttpException('User already exists', 404);
  }

  const hashPass = await bcrypt.hash(password, 12);

  user = await User.create({ login, password: hashPass });

  const token = jwt.sign({ user }, config.app, {
    expiresIn: config.jwt_ttl,
  });

  return {
    token,
    userId: user.id,
  };
}

module.exports = {
  loginUser,
  registerUser,
};
