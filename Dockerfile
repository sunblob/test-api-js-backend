FROM node:16-alpine as production

WORKDIR /app

COPY package*.json ./
COPY yarn.lock ./
COPY .eslintrc.js ./
COPY jest.config.js ./
COPY . ./

RUN yarn install --frozen-lockfile --only=production && \
    yarn lint

CMD ["yarn", "start"]
