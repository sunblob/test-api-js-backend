const cron = require('node-cron');
const fs = require('fs');

const createApp = require('./utils/create-app');

require('dotenv').config();

const app = createApp();

const config = require('./config.json');
const logger = require('./services/logger')(module);

// Подключение к БД
const dbConnect = require('./utils/db-connect');

cron.schedule('0 * * * *', () => {
  fs.rm('./public/images/', { recursive: true, force: true }, (err) => {
    if (err) logger(err);
  });
});

async function start() {
  try {
    app.listen(config.port, () => {
      logger.info(`App has been started on port ${config.port}...`);
    });

    await dbConnect();
  } catch (error) {
    logger.error(error.message);
    process.exit(1);
  }
}

start();

module.exports = app;
