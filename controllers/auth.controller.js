const Joi = require('joi');

const asyncHandler = require('../middleware/async-handler.middleware');
const { loginUser, registerUser } = require('../services/auth.service');

const login = asyncHandler(async (req, res) => {
  const loginSchema = Joi.object({
    login: Joi.string().required(),
    password: Joi.string().min(6).required(),
  });

  const value = await loginSchema.validateAsync(req.body);

  const { token, userId } = await loginUser(value.login, value.password);

  return res.json({
    accessToken: token,
    userId,
  });
});

const register = asyncHandler(async (req, res) => {
  const loginSchema = Joi.object({
    login: Joi.string().required(),
    password: Joi.string().min(6).required(),
  });

  const value = await loginSchema.validateAsync(req.body);

  const { token, userId } = await registerUser(value.login, value.password);

  return res.json({
    accessToken: token,
    userId,
  });
});

module.exports = {
  login,
  register,
};
