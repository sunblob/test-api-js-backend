const Joi = require('joi');

const asyncHandler = require('../middleware/async-handler.middleware');
const {
  del, update, create, get, getAll,
} = require('../services/companies.service');

const getCompany = asyncHandler(async (req, res) => {
  const { id } = req.params;

  const company = await get(id);

  return res.json(company);
});

const getCompanies = asyncHandler(async (req, res) => {
  // Делаем массив, если пришла 1 строка
  if (req.query.sorting && !Array.isArray(req.query.sorting)) {
    req.query.sorting = [...req.query.sorting];
  }

  // Валидация квери параметров
  const querySchema = Joi.object({
    sorting: Joi.array().items(Joi.string().valid('name', '-name', 'createdAt', '-createdAt')).default(['createdAt']),
    filter: Joi.object({
      status: Joi.string().valid('active', 'other'),
      type: Joi.string().valid('agent', 'contractor'),
    }).default({}),
    page: Joi.number().default(0),
    limit: Joi.number().default(10),
  });

  const queryValue = await querySchema.validateAsync(req.query);

  const companies = await getAll(queryValue);

  return res.status(200).json(companies);
});

const createCompany = asyncHandler(async (req, res) => {
  // Валидация request body
  const createSchema = Joi.object({
    contactId: Joi.string().hex().length(24).required(),
    name: Joi.string().required(),
    shortName: Joi.string().required(),
    businessEntity: Joi.string().required(),
    type: Joi.string().valid('agent', 'contractor').required(),
    status: Joi.string().valid('active').default('active'),
    contract: Joi.object({
      no: Joi.string().required(),
      issue_date: Joi.date().default(Date.now()),
    }),
    address: Joi.string(),
  });

  const createValue = await createSchema.validateAsync(req.body);

  const company = await create(createValue);

  return res.status(200).json(company);
});

const updateCompany = asyncHandler(async (req, res) => {
  const updateSchema = Joi.object({
    name: Joi.string(),
    shortName: Joi.string(),
    businessEntity: Joi.string(),
    type: Joi.string().valid('agent', 'contractor'),
    status: Joi.string().valid('active'),
    contract: Joi.object({
      no: Joi.string(),
      issueDate: Joi.date(),
    }),
    address: Joi.string(),
  });

  const updValue = await updateSchema.validateAsync(req.body);

  const { id } = req.params;

  const company = await update(id, updValue);

  return res.status(200).json(company);
});

const deleteCompany = asyncHandler(async (req, res) => {
  const { id } = req.params;

  await del(id);

  return res.status(200).end();
});

// function _getCurrentURL(req) {
//   const { port } = config;
//   return `${req.protocol}://${req.hostname}${port === '80' || port === '443' ? '' : `:${port}`}/`;
// }

module.exports = {
  getCompanies,
  updateCompany,
  createCompany,
  deleteCompany,
  getCompany,
};
