const Joi = require('joi');
const asyncHandler = require('../middleware/async-handler.middleware');
const {
  get, getAll, create, update, del,
} = require('../services/contacts.service');

const getContact = asyncHandler(async (req, res) => {
  const { id } = req.params;

  const contact = await get(id);

  return res.status(200).json(contact);
});

const getContacts = asyncHandler(async (req, res) => {
  const contacts = await getAll();

  res.status(200).json(contacts);
});

const createContact = asyncHandler(async (req, res) => {
  const createSchema = Joi.object({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    patronymic: Joi.string().default(''),
    phone: Joi.string().required(),
    email: Joi.string().email().required(),
  });

  const value = await createSchema.validateAsync(req.body);

  const contact = await create(value);

  res.status(200).json(contact);
});

const updateContact = asyncHandler(async (req, res) => {
  const { id } = req.params;

  const updateSchema = Joi.object({
    firstname: Joi.string(),
    lastname: Joi.string(),
    patronymic: Joi.string(),
    phone: Joi.string(),
    email: Joi.string().email(),
  });

  const value = await updateSchema.validateAsync(req.body);

  const contact = await update(id, value);

  res.status(200).json(contact);
});

const deleteContact = asyncHandler(async (req, res) => {
  const { id } = req.params;

  await del(id);

  return res.status(200).end();
});

module.exports = {
  getContact,
  getContacts,
  updateContact,
  createContact,
  deleteContact,
};
